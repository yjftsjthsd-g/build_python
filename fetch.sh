#!/bin/sh

VERSION="${1:?need version}"

git clone --branch ${VERSION} --depth 1 https://github.com/python/cpython.git

