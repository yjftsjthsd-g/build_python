# build_python

Builds assorted versions of Python.


## Use

Just go to the latest pipeline, find the version you want, download the tarball
(or download the zip and extract it), extract so the python directory ends up in
`/opt`, and probably symlink like
```
sudo ln -svi /opt/python-3.10/bin/python /usr/local/bin/python3.10
```

## TODO

* What happens at runtime if optional libraries are absent?
* Actually test installing a few version and using them.
* Figure out pip, etc.
* What is `./configure --enable-optimizations` and should I enable it?


## License

*This* repo is under the unlicense, but of course this repo is just a bunch of
build scripts for CPython, which is under its own license which will apply to
generated artifacts.

