#!/bin/sh

VERSION="${1:?need version}"
PREFIX="${PREFIX:-/opt/python-${VERSION}}"
#CONFIGOPTS="${CONFIGOPTS:---foo"

echo "Building cpython version ${VERSION} for ${PREFIX}"

cd cpython && ./configure --prefix=${PREFIX} ${CONFIGOPTS} && make

